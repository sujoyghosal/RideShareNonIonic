cordova.define('cordova/plugin_list', function(require, exports, module) {
module.exports = [
    {
        "file": "plugins/com.phonegap.plugins.PushPlugin/www/PushNotification.js",
        "id": "com.phonegap.plugins.PushPlugin.PushNotification",
        "pluginId": "com.phonegap.plugins.PushPlugin",
        "clobbers": [
            "PushNotification"
        ]
    },
    {
        "file": "plugins/com.phonegap.plugins.PushPlugin/src/windows/PushPluginProxy.js",
        "id": "com.phonegap.plugins.PushPlugin.PushPlugin",
        "pluginId": "com.phonegap.plugins.PushPlugin",
        "merges": [
            ""
        ]
    },
    {
        "file": "plugins/org.apache.cordova.device/www/device.js",
        "id": "org.apache.cordova.device.device",
        "pluginId": "org.apache.cordova.device",
        "clobbers": [
            "device"
        ]
    },
    {
        "file": "plugins/org.apache.cordova.device/src/windows/DeviceProxy.js",
        "id": "org.apache.cordova.device.DeviceProxy",
        "pluginId": "org.apache.cordova.device",
        "merges": [
            ""
        ]
    },
    {
        "file": "plugins/de.appplant.cordova.plugin.local-notification/www/local-notification.js",
        "id": "de.appplant.cordova.plugin.local-notification.LocalNotification",
        "pluginId": "de.appplant.cordova.plugin.local-notification",
        "clobbers": [
            "cordova.plugins.notification.local",
            "plugin.notification.local"
        ]
    },
    {
        "file": "plugins/de.appplant.cordova.plugin.local-notification/www/local-notification-core.js",
        "id": "de.appplant.cordova.plugin.local-notification.LocalNotification.Core",
        "pluginId": "de.appplant.cordova.plugin.local-notification",
        "clobbers": [
            "cordova.plugins.notification.local.core",
            "plugin.notification.local.core"
        ]
    },
    {
        "file": "plugins/de.appplant.cordova.plugin.local-notification/www/local-notification-util.js",
        "id": "de.appplant.cordova.plugin.local-notification.LocalNotification.Util",
        "pluginId": "de.appplant.cordova.plugin.local-notification",
        "merges": [
            "cordova.plugins.notification.local.core",
            "plugin.notification.local.core"
        ]
    },
    {
        "file": "plugins/de.appplant.cordova.plugin.local-notification/src/windows/LocalNotificationProxy.js",
        "id": "de.appplant.cordova.plugin.local-notification.LocalNotification.Proxy",
        "pluginId": "de.appplant.cordova.plugin.local-notification",
        "merges": [
            ""
        ]
    },
    {
        "file": "plugins/de.appplant.cordova.plugin.local-notification/src/windows/LocalNotificationCore.js",
        "id": "de.appplant.cordova.plugin.local-notification.LocalNotification.Proxy.Core",
        "pluginId": "de.appplant.cordova.plugin.local-notification",
        "merges": [
            ""
        ]
    },
    {
        "file": "plugins/de.appplant.cordova.plugin.local-notification/src/windows/LocalNotificationUtil.js",
        "id": "de.appplant.cordova.plugin.local-notification.LocalNotification.Proxy.Util",
        "pluginId": "de.appplant.cordova.plugin.local-notification",
        "merges": [
            ""
        ]
    }
];
module.exports.metadata = 
// TOP OF METADATA
{
    "com.phonegap.plugins.PushPlugin": "2.5.0",
    "cordova-plugin-registerusernotificationsettings": "1.0.2",
    "cordova-plugin-whitelist": "1.2.0",
    "org.apache.cordova.device": "0.3.0",
    "de.appplant.cordova.plugin.local-notification": "0.8.3-dev"
}
// BOTTOM OF METADATA
});